package com.tcx.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.tcx.demo.model.Book;

public interface BookRepository extends CrudRepository<Book, Long> {

}
